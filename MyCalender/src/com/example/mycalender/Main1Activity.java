package com.example.mycalender;

import hirondelle.date4j.DateTime;

import java.util.HashMap;
import java.util.Map;
import java.util.Random;

import android.app.Activity;
import android.content.Context;
import android.graphics.Color;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.example.mycalender.MyCalenderView.ItemClickListener;
import com.example.mycalender.MyCalenderView.ViewFactory;

public class Main1Activity extends Activity implements ItemClickListener, ViewFactory {
	private MyCalenderView myCalenderView1;
	private TextView tvCalTitle;

	private Map<DateTime,Integer> myData;		//数据集
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main1);
		myCalenderView1 = (MyCalenderView) findViewById(R.id.myCalenderView1);
		myCalenderView1.setOnItemClickListener(this);
		myCalenderView1.setViewFactory(this);
		
		tvCalTitle = (TextView) findViewById(R.id.tvCalTitle);
		updateTvCal();
		
		myData=new HashMap<DateTime, Integer>();
		getTestData(null);
	}
	
	public void getTestData(View v) {//添加测试数据
		myData.clear();
		Random r=new Random(System.currentTimeMillis());
		DateTime today=DateTime.forDateOnly(2013, 11, 2);
		for (int i = 0; i < 40; i++) {
			myData.put(today.plusDays(i), Color.rgb(r.nextInt(255), r.nextInt(255), r.nextInt(255)));
		}
		myCalenderView1.reflashCalendar();
	}
	
	/**上一个月*/
	public void goPervMonth(View v){
		myCalenderView1.pervMonth();
		updateTvCal();
	}
	/**下一个月*/
	public void goNextMonth(View v){
		myCalenderView1.nextMonth();
		updateTvCal();
	}
	
	public void updateTvCal(){
		tvCalTitle.setText(myCalenderView1.getYear()+"年"+myCalenderView1.getMonth()+"月");
	}

	@Override
	public void onItemClick(int year, int month, int day,
			boolean isInCurrentMonth) {
		if(isInCurrentMonth){
			Toast.makeText(this, "点击了:"+year+"-"+month+"-"+day+",颜色值:"+myData.get(DateTime.forDateOnly(year, month, day)), Toast.LENGTH_SHORT).show();
		}
	}

	@Override
	public View getCellView(Context context,int year, int month, int day,
			boolean isInCurrentMonth) {
		TextView tv=myCalenderView1.getDefaultCellView(year, month, day, isInCurrentMonth);
		Integer bgColor=myData.get(DateTime.forDateOnly(year, month, day));
		if(bgColor!=null){
			tv.setBackgroundColor(bgColor.intValue());
		}
		return tv;
	}
	
}
